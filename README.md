#la buffered haikus

This short project is a few experiments based on a prompt to create a application to create haikus from user input

I started with the angular-seed project as a basis for the application, to get up and running quickly, I then sketched a solution for parsing user input on paper. It's basically:

1. Determine the character for the delineation of the words in the list (expecting common ones like line-breaks, commas, pipes...)
2. Determine the character for the separation of the syllables (expecting one of the standardized ones like hyphen, interpunct, hyphenation point...)
3. Create list of words with their number of syllables (e.g. {word: 'hello', syllables:2})
4. Generate lines based on this data, randomly filling a line with words that will fit until it's full

**You can skip to /app/userInput/userInput.js to see it
**

Some assumptions:

1. It's ok to repeat words from the list
2. There will be at least one word provided in the list
3. The list will conform to the word and syllable delineation schemes above
4. In the case of a list of words all too large for a given line, that line will be buffered with (presumably musical) "la"s.
5. Will not convert numbers (e.g. 1 = one,  17 = se-ven-teen)

Some Known Issues

1.  Active tab selection is wonky, Bootstrap was a late addition
2.  The "la" buffer decision was made without consulting the "customer", it was made for expediency's sake
3.  No meaningful tests... did not TDD this

-C G Gillotti

#Install

Get the source then:

```
#!cmd


npm install
npm start
```

It should spin up on localhost:8000/app

#Sections:

## User Input Section

This is where a user can put in their words and generate haikus, with all of the assumptions above.

## Simple Dictionary Section

I actually started this project with this, a section for generating from a canned list of {word: "word", syllableCount:1}

This helped me feel out implementing the screen, and gave me a good first run of making and displaying haikus

## Using Generated Library

I wanted to see how this worked with a larger set of data so I downloaded a JSON formatted list of words and ran them through a node application that parses syllables, generating a similar object as in Simple

The node application is under the data directory and requires the use of the [Syllable](https://www.npmjs.com/package/syllable) library by Titus Wormer.

I also noticed some swear words in the dictionary that I got so I added a filter to the generator to leave out the bad words.

I use that generated dictionary instead of the canned on in "Simple"

Slightly more complex, and it gave me a reason to experiment with moving the haiku display to its own directive