'use strict';

angular.module('myApp.userInput', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/userInput', {
    templateUrl: 'userInput/userInput.html',
    controller: 'UserInputCtrl'
  });
}])

.controller('UserInputCtrl', function($scope) {
   var self = this;
   $scope.userInput = "oak,leaf,pine,wind,blows,branch,owl,ivy,moon-light,tra-verse,dark-en-ing,sun-rise,sha-dow,moun-tains,scorp-i-ans,live,float,de-pose,con-cern,all-i-gat-or,zen";
   var words = {};

        //This will generate a line of a specified length using the words the user entered
        //it's scoped this way to be passed to the haikuDisplay directive
        //I'm padding lines with la, if there are too large of words provided
    $scope.GenerateLine =  function (length){
        if($scope.userInput === "")
        {return;}

        words = GenerateDictionary($scope.userInput);
        var constructedLine = "";
        var filteredSet = words.filter(function(item){
            return item.syllableCount <= length;
        });

        if(filteredSet.length != 0) {
            for (var i = length; i > 0;) {
                filteredSet = filteredSet.filter(function (item) {
                    return item.syllableCount <= i;
                });

                if(filteredSet.length > 0) {
                    var random = Math.floor(Math.random() * filteredSet.length);

                    $scope.randomIndex = random;
                    if (filteredSet[random].syllableCount <= i) {
                        constructedLine = constructedLine.concat(filteredSet[random].word + " ");
                        i -= filteredSet[random].syllableCount;
                    }
                }else{
                    constructedLine += "la";
                    i--;
                }

            }
        }else{
            for(var i = 0; i < length; i++)
            {
                constructedLine += "la ";
            }
        }
        return constructedLine
    }

    var GenerateDictionary = function(input){
        var syllableSpacer = GetSyllableSpacer(input);
        var wordSpacer = GetWordSpacer(input);
        return CreateDictionaryFromInput(input, syllableSpacer,wordSpacer);
    };

    var CreateDictionaryFromInput = function(input, syllableSpacer,wordSpacer)
        {
            var wordList = input.split(wordSpacer);
             wordList.map(function(item){
                var splitWord = item.split(syllableSpacer);
                return {word: splitWord.join(''), syllableCount: splitWord.length};
             });

            return         wordList.map(function(item){
                var splitWord = item.split(syllableSpacer);
                return {word: splitWord.join(''), syllableCount: splitWord.length};
            });;
        }

    var GetSyllableSpacer = function (input)
    {
        if(input.indexOf('·') >= 0 ) {
            return '·';
        }
        else if ( input.indexOf('‧') >= 0) {
            return '‧';
        }
        else if(input.indexOf('-') >= 0) {
            return '-';
        }
        else{
            return '-';
        }
    };

    var GetWordSpacer = function(input)
    {
        if(input.indexOf(',') >= 0)
        {
            return ',';
        }else if (input.indexOf('|') >= 0)
        {
            return '|';
        }else if (input.indexOf('\n') >= 0)
        {
            return '\n'
        }else{
            return ';';
        }
    };
});