'use strict';

// Declare app level module which depends on views, and components

    //// horrible words pulled from https://github.com/web-mech/badwords
    //Copyright (c) 2013 Michael Price

angular.module('myApp', [
  'ngRoute',
  'myApp.simpleDictionary',
  'myApp.haikuDisplayDirective',
  'myApp.syllableLib',
    'myApp.userInput',
    'myApp.about',
  'myApp.version'
]).
config(['$routeProvider', function($routeProvider) {
  $routeProvider.otherwise({redirectTo: '/userInput'});
}])
    .filter('poemCasing', function(){
        return function(stringToFormat){
            if(stringToFormat && stringToFormat.length >0) {
                return stringToFormat[0].toUpperCase() + stringToFormat.substring(1).toLowerCase();
            }else{
                return '';
            }
        };

    });
