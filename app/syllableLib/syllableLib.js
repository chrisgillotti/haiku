'use strict';

// Depending on a dictionary created from a list of words processed by
//  Syllable : https://www.npmjs.com/package/syllable by wooorm (Titus Wormer)

angular.module('myApp.syllableLib', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/syllableLib', {
    templateUrl: 'syllableLib/syllableLib.html',
    controller: 'SyllableLibCtrl'
  });
}])

.controller('SyllableLibCtrl', function($scope, $http) {
        var self = this;
        var words = {};

        $scope.DictionaryLoaded = false;

        $scope.GenerateLine =  function (length){

            var constructedLine = "";
            var filteredSet = words.filter(function(item){
                return item.syllableCount <= length;
            });

            for(var i = length; i > 0; )
            {
                filteredSet = filteredSet.filter( function(item){
                    return item.syllableCount <= i;
                });

                var random = Math.floor( Math.random() * filteredSet.length);

                $scope.randomIndex = random;
                if(filteredSet[random].syllableCount <= i) {
                    constructedLine = constructedLine.concat(filteredSet[random].word + " ");
                    i -= filteredSet[random].syllableCount;
                }
            }
            return constructedLine

    };

        $http.get('data/syllabledictionary.json').then(function(res){
            words = res.data;
            $scope.DictionaryLoaded = true;

        }, function(err){
            alert(err);
        });



});