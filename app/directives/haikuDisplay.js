//  Filter based on solution from http://stackoverflow.com/users/1328461/dubadub


angular.module('myApp.haikuDisplayDirective', [])
    .directive('haikuDisplay', function() {
        return {
            restrict: 'E',
            templateUrl: 'directives/haikuDisplay.html',

            scope:{
            generate: '&'
              },
            controller: function($scope){
                $scope.firstLineLength = 5;
                $scope.secondLineLength = 7;
                $scope.thirdLineLength = 5;

                $scope.firstLine = "";
                $scope.secondLine = "";
                $scope.thirdLine = "";

                $scope.hasPoem = false;

                $scope.getHaiku = function(){
                    $scope.firstLine = $scope.generate({length: $scope.firstLineLength});
                    $scope.secondLine = $scope.generate({length: $scope.secondLineLength});
                    $scope.thirdLine = $scope.generate({length: $scope.thirdLineLength});
                    $scope.hasPoem = true;
                }


            }
        };
    });