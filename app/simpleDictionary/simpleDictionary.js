'use strict';

angular.module('myApp.simpleDictionary', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/simpleDictionary', {
    templateUrl: 'simpleDictionary/simpleDictionary.html',
    controller: 'SimpleDictionaryCtrl'
  });
}])

.controller('SimpleDictionaryCtrl', function($scope) {
   var self = this;


    $scope.GenerateLine =  function (length){

       var constructedLine = "";
       var filteredSet = words.filter(function(item){
           return item.syllableCount <= length;
       });

       for(var i = length; i > 0; )
        {
             filteredSet = filteredSet.filter( function(item){
               return item.syllableCount <= i;
            });

            var random = Math.floor( Math.random() * filteredSet.length);

            $scope.randomIndex = random;
            if(filteredSet[random].syllableCount <= i) {
                constructedLine = constructedLine.concat(filteredSet[random].word + " ");
                i -= filteredSet[random].syllableCount;
            }
        }

        return constructedLine
    }

   var words =
   [{
       syllableCount: 1,
       word: "hand"
   },{
       syllableCount: 2,
       word: "lately"
   },{
       syllableCount: 2,
       word: "trending"
   },{
       syllableCount: 4,
       word: "application"
   },{
       syllableCount: 1,
       word: "bask"
   },{
       syllableCount: 2,
       word: "bacon"
   },{
       syllableCount: 3,
       word: "tippled"
   },{
       syllableCount: 4,
       word: "salmon"
   },{
       syllableCount: 5,
       word: "uncomplicated"
   },{
       syllableCount: 1,
       word: "foot"
   },{
       syllableCount: 2,
       word: "tires"
   },{
       syllableCount: 3,
       word: "depleted"
   },{
       syllableCount: 3,
       word: "popovers"
   },{
       syllableCount: 4,
       word: "revolution"
   },{
       syllableCount: 1,
       word: "can"
   },  {
       syllableCount: 1,
       word: "bask"
   },{
       syllableCount: 2,
       word: "bacon"
   },{
       syllableCount: 3,
       word: "oak tree bark"
   },{
       syllableCount: 4,
       word: "salmon"
   },{
       syllableCount: 5,
       word: "uncomplicated"
   },{
       syllableCount: 1,
       word: "foot"
   },{
       syllableCount: 2,
       word: "tires"
   },{
       syllableCount: 3,
       word: "depleted"
   },{
       syllableCount: 3,
       word: "popovers"
   },{
       syllableCount: 4,
       word: "revolution"
   },{
       syllableCount: 1,
       word: "can"
   }, {
       syllableCount: 1,
       word: "bask"
   },{
       syllableCount: 2,
       word: "bacon"
   },{
       syllableCount: 3,
       word: "oak tree bark"
   },{
       syllableCount: 4,
       word: "salmon"
   },{
       syllableCount: 5,
       word: "uncomplicated"
   },{
       syllableCount: 1,
       word: "foot"
   },{
       syllableCount: 2,
       word: "tires"
   },{
       syllableCount: 3,
       word: "depleted"
   },{
       syllableCount: 3,
       word: "popovers"
   },{
       syllableCount: 4,
       word: "revolution"
   },{
       syllableCount: 1,
       word: "can"
   }   ]

});