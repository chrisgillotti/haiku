#!/usr/bin/env node


var syllable = require('syllable');
var fs = require('fs');

var dictionaryPath = process.argv[2] || './mydictionary.json';
var badWordsPath = process.argv[3] || './horribleWords.json';
var dictionary = JSON.parse(fs.readFileSync(dictionaryPath, 'utf8'));
var terribleWordsList = JSON.parse(fs.readFileSync(badWordsPath, 'utf8'));

var terribleWords = {};
terribleWordsList.words = terribleWordsList.words ||[];

terribleWordsList.words.forEach(function(item){
    terribleWords[item.toLowerCase()] = "gross";
});


dictionary = dictionary.map(function(item){
    if(terribleWords[item.toLowerCase()] === undefined) {
        return {word: item, syllableCount: syllable(item)}
    }else{
      //  console.log("Not saving: " + item);
    }

});

dictionary = dictionary.filter(function(word){return word != undefined;});
//console.log(dictionary);
fs.writeFile('./syllabledictionary.json',JSON.stringify(dictionary));


console.log(dictionaryPath);